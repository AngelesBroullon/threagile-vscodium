# Change Log

All notable changes to the "threagile-snippets" extension will be documented in this file.


## [0.0.1] - 2021-05-29

### Added
* Base stubs added
  * Threagile model base
  * Threagile data asset
  * Threagile technical asset
  * Threagile communication link
  * Threagile trust boundary
  * Threagile shared runtime
  * Threagile individual risk
  * Threagile risk tracking
* Data assets examples
  * Customer contracts
  * Customer contract summaries
  * Customer operational data
  * Customer accounts
  * Internal business data
  * Client application code
  * Server application code
  * Build job configuration
  * Marketing material
  * ERP logs
  * ERP customizing data
  * Database customizing and dumps
* Technical assets examples
  * Customer web client
  * Backoffice client
  * Backend admin client
  * Load Balancer
  * Apache Webserver
  * Identity provider
  * LDAP auth server
  * Marketing CMS
  * Backoffice ERP system
  * Contract fileserver
  * Customer Contract Database
  * External development client
  * Git repository
  * Jenkins buildserver
* Trust boundaries assets examples
  * Web DMZ
  * ERP DMZ
  * Application Network
  * Auth Handling Environment
  * Auth Dev Network
* Shared runtimes assets examples
  * WebApp and backoffice virtualization
* Individual risks assets examples
  * Individual Generic Risk
* Risk tracking assets examples
  * Untrusted Deserialization
  * LDAP injection
  * Unencrypted asset
  * Missing authentication
  * Missing hardening
  * DOS risky accesess

## [0.0.2] - 2021-05-31

### Added
* Data assets examples
  * AWS Cloudwatch Logs
  * AWS S3 Logs
* Technical assets examples
  * AWS Web Application Firewall
  * AWS Application Load Balancer
  * AWS EC2 instance with App
  * AWS Cloudwatch
  * AWS S3 bucket
  * AWS CloudFront
  * AWS API Gateway
* Communication links
  * AWS Application Load Balancer
  * AWS EC2 instance with App
  * AWS CloudWatch Logs
  * AWS S3 Bucket logs
  * Apache Webserver
  * CMS Content Traffic
  * ERP Internal Access
  * Marketing CMS
  * SQL database
  * LDAP Auth Server
* Trust boundaries assets examples
  * AWS Public Subnet
  * AWS Private Subnet

## [0.0.3] - 2021-11-07

### Added

* Correct typos on snippets.
* Added commands to the command palette:
  * New commands:
    * `Threagile: Retrieve model`: retrieves model stubfrom server.
    * `Threagile: Analyze model`: sends the file to server, and retrieves the zip with the results.
  * Recognizes the following VScodium settings, having these default values if undefined:
    * `"threagile.defaultDir": "threagile"`: name of the directory which will be created to download the results.
    * `"threagile.apiUrl": "https://run.threagile.io"`: the Threagile server you will be working against.

## [0.0.4] - 2021-11-18

### Added

* Added Risk identified base snippet.
* Added suggested values.
* Added Global Network snippet.

## [0.0.5] - 2022-07-25

### Added

* Added Secrets Manager snippet.
* Added Secrets Manager data snippet.
* Added lambda snippet.

## [0.0.6] - 2022-11-16

### Added
* Added AWS CodeBuild snippet.
* Added AWS Codepipeline snippet.
* Added AWS Cloud Data Layer snippet (shared runtime).
* Added AWS VPC endpoint (nested trust boundary).

## [0.0.7] - 2023-02-03

### Added
* Added AWS DynamoDB snippet.

## [0.0.8] - 2023-04-20

### Added
* Added AWS Glue snippet.
* Added AWS Sagemaker snippet.
* Updated AWS lambda snippet.