# Threagile snippets

The goal of this extension is to provide snippets to increase your productivity when working with the [Threagile](https://threagile.io/) toolbox on [Visual Studio Codium](https://vscodium.com/).

You may download the latest [VSIX package](https://gitlab.com/AngelesBroullon/threagile-vscodium/-/jobs/artifacts/master/download?job=vsix-file), built on GitLab-CI, so you can install it with ease.


## Features

### Snippets

Snippets are invoked by typing the prefix plus using the autocomplete function shortcut (default keys: Ctrl + Spacebase).

![Screenshot of the snippets being called](images/screenshot-threagile.png)

They have been split on several file, as modules, in order to make them easier to track and update.
* Base stubs are interative, inviting you to fill up the fields.
* Example assets are currently not interactive.

Snippet prefix call | Snippet module called
--                  | --
`!ta-base-`         | Base stubs
`!ta-data-`         | Data example assets
`!ta-tech-`         | Technical example assets
`!ta-comm-link-`    | Communication links example assets
`!ta-trust-`        | Trust boundaries example assets
`!ta-shared-`       | Shared runtimes example assets
`!ta-ind-`          | Individual risks example assets
`!ta-risk-`         | Risk tracking example assets

## Commands

The extension adds to new commands to your palette:
*  `Threagile: Retrieve mode`: Retrieves a stub from the Threagile server.
*  `Threagile: Analyze model`: Uploads a yaml file to the Threagile server.

## Quick how to

### Snippets

1. Start by creating the base model stub on a YAML file, by typing:
  ```bash
  !ta-base-model-base
  ```
2. Continue by filling up teach of the assets (data, technical...).
  * You can `!ta-base-` for each one of them.
  * You can use any of the other modules which has pre-filled examples.

### Commands

1. Invoke the command palette (default: `F1` key).
2. Type `Threagile: Retrieve mode` or `Threagile: Analyze model`.


## Extension settings on the IDE

* There are 2 parameters which this extension recognises:
  * `threagile.defaultDir`: name of the sub-directory where the threagile results will be downloaded.
  * `download-file.apiUrl`: the threagile server instance you may work against.

* The following are the default values that will be filled out inside the extension if the are not defined.
  ```js
  "threagile.defaultDir": "threagile",
  "threagile.apiUrl": "https://run.threagile.io"
  ```

## Requirements and extras

You may also be interested in configuring your code workspace to get extra autocompletion plus validations. This is highly recommended as the current status of the analysis on server does not provide the full results of the syntax and semantic errors.

1. Install the [Red Hat YAML support extension](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml).
2. Configure it in your code workspace, following the structure `"schema_location": "pattern_of_files_who_will_be_checked".`
  ```
  "yaml.schemas": {
    "https://run.threagile.io/schema.json": "*.threagile.yaml",
  },
  ```

## Known Issues

* This extension is still work in progress. There will be more assets added (e.g. AWS assets).


## Release Notes

You can check the `CHANGELOG.md` file on this project repository.