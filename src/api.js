const vscode = require('vscode');
const https = require("https");
const fs = require("fs");

const DEFAULT_API = "run.threagile.io";
const DEFAULT_DIR = "threagile";

/**
 * Download the stub file from a threagile server.
 * @param {vscode.OutputChannel} outputChannel the vscode output channel tab.
 */
async function directStub(outputChannel) {
  let configParams = vscode.workspace.getConfiguration('threagile');
  let defaultDir = vscode.workspace.workspaceFolders[0].uri.fsPath;
  console.log(defaultDir);
  let apiUrl = configParams.get('apiUrl') === undefined ? DEFAULT_API : configParams.get('apiUrl');
  apiUrl =  apiUrl.startsWith('https://') ? apiUrl : "https://" + apiUrl;

  https.get(`${apiUrl}/direct/stub`, (response) => {
    let file = fs.createWriteStream(defaultDir + `\\threagile.yaml`);
    response.pipe(file);

    file.on('finish', () => {
      file.close();
      vscode.window.showInformationMessage("File was downloaded");
      outputChannel.append("File was downloaded into " + defaultDir + `\\threagile.yaml`);
    });
  }).on("error", (err) => {
    vscode.window.showErrorMessage("There was an error while downloading: " + err.message);
    outputChannel.append("There was an error while downloading: " + err.message);
    outputChannel.show();
  });
}

/**
 * Calls to Threagile API, to analyze the current file.
 * @param {vscode.OutputChannel} outputChannel the vscode output channel tab.
 */
async function directAnalyze(outputChannel) {
  let configParams = vscode.workspace.getConfiguration('threagile-file');
  let defaultDir = configParams.get('defaultDir') === undefined ? DEFAULT_DIR : configParams.get('defaultDir');
  defaultDir = vscode.workspace.workspaceFolders[0].uri.fsPath + '\\' + defaultDir;
  let apiUrl = configParams.get('apiUrl') === undefined ? DEFAULT_API : configParams.get('apiUrl');
  apiUrl =  apiUrl.startsWith('https://') ? apiUrl.substr(8, apiUrl.length) : apiUrl;
  let currentFilePath = vscode.window.activeTextEditor.document.fileName;
  validateDestinationDirectory(defaultDir);
  let outputfile = defaultDir + `\\threagile.zip`;

  var options = {
    'method': 'POST',
    'hostname': apiUrl,
    'path': '/direct/analyze?dpi=120',
    'headers': {
      'accept': 'application/zip',
    }
  };
  var postData = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n" +
    "Content-Disposition: form-data; name=\"file\"; filename=\"" +
    currentFilePath + "\"\r\n" +
    "Content-Type: \"multipart/form-data\"\r\n" +
    "\r\n" + fs.readFileSync(currentFilePath) + "\r\n" +
    "------WebKitFormBoundary7MA4YWxkTrZu0gW--";

  vscode.window.withProgress({
    location: vscode.ProgressLocation.Notification,
    title: "Analyzing the threagile",
    cancellable: true
  }, async (progress) => {
    await doRequestPost(options, postData, outputfile, progress, outputChannel);
    outputChannel.append("Analysis finished, you may check the results.");
    vscode.window.showInformationMessage("Analysis finished, you may check the results.");
  });
}

/**
 * Validates the destination directory
 * - If it does not exists, it creates it
 * @param {string} directory the directory adress
 */
function validateDestinationDirectory(directory) {
  if (!fs.existsSync(directory)) {
    fs.mkdir(directory, (err) => {
      if (err) {
        throw err;
      }
    });
  }
}

/**
 * Does a POST request and pipes the data into a file passed as a parameter.
 * @param {json} options options field for POST REQUEST via https native library on nodejs.
 * @param {string} postData the data field for POST REQUEST via https native library on nodejs.
 * @param {string} outputfile the file where we want to pipe the result of the query.
 * @param {vscode.Progress} progress the progress bar.
 * @param {vscode.OutputChannel} outputChannel the vscode output channel tab.
 */
async function doRequestPost(options, postData, outputfile, progress, outputChannel) {
  return new Promise((resolve, reject) => {
    var request = https.request(options, function (response) {
      var chunks = [];
      response.on("data", function (chunk) {
        progress.report({ increment: 5 });
        setTimeout(() => {
          progress.report({ increment: 10, message: "Downloading file..." });
        }, 1000);
        chunks.push(chunk);
      });
      response.on("end", function () {
        processResponse(response, chunks, outputfile, outputChannel);
        progress.report({ increment: 100, message: "Analysis completed"});
        resolve("Analysis completed");
      });
      response.on("error", function (error) {
        vscode.window.showErrorMessage("There was an error while downloading:\n " + error.message);
        outputChannel.append("There was an error while downloading:\n " + error.message);
        outputChannel.show();
        reject(new Error(`Failed to do the analysis (${response.statusCode})`));
      });
    });

    try {
      request.setHeader('content-type', 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW');
      request.write(postData);
      request.end();
    } catch (error) {
      console.error(error);
      vscode.window.showErrorMessage("There was a process error!");
    }
  });
}

/**
 * Processes the response of the POST request.
 * @param {IncomingMessage} response the result of the POST request we need to process
 * @param {any []} chunks a buffer with all the chunks from the request 
 * @param {string} outputfile the file where we want to pipe the result of the query.
 * @param {vscode.OutputChannel} outputChannel the vscode output channel tab.
 */
function processResponse(response, chunks, outputfile, outputChannel) {
  var responseBody = Buffer.concat(chunks);
  if (response.statusCode !== 200){
    outputChannel.append("There was an error on the process:\n" +
      responseBody.toString().error);
    outputChannel.show();
    vscode.window.showWarningMessage("There was an error on the process:\n" +
      responseBody.toString().error);
  } else {
    fs.writeFile(outputfile, responseBody, (err) => {
      if(!err){
        outputChannel.append("Analysis results are on file " + outputfile);
        vscode.window.showInformationMessage("Analysis results are on file " + outputfile);
      }
    });
  }
}

module.exports = {
  directStub,
  directAnalyze
}