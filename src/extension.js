const vscode = require('vscode');
const api = require('./api');
const outputChannel = vscode.window.createOutputChannel(`Threagile API`);

/**
 * Extension activation process: register the commands.
 * @param {vscode.ExtensionContext} context the context for adding the new commands.
 */
function activate(context) {
	console.log('The Threagile extension is now active!');

	let getModel = vscode.commands.registerCommand('threagile-vscodium.getModel', retriveFromServer);
	context.subscriptions.push(getModel);

	let analyzeModel = vscode.commands.registerCommand('threagile-vscodium.analyzeModel', sendToServer);
	context.subscriptions.push(analyzeModel);
}

/**
 * Retrieves the model stub from the Threagile server.
 * It writes that content in the current file if it has yaml extension.
 */
function retriveFromServer(){
	if (!currentFileIsYaml()) {
		vscode.window.showErrorMessage('Current file is not YAML');
	} else {
		api.directStub(outputChannel);
	}
}

/**
 * Sends the current model to the server.
 * It will retrieve the currently active file to the server,
 * if it has a valid YAML extension.
 */
function sendToServer() {
	if (!currentFileIsYaml()) {
		vscode.window.showErrorMessage('Current file is not YAML');
	} else {
    api.directAnalyze(outputChannel);
	}
}

/**
 * Checks if the currently active file has a yml or yaml extension.
 * @returns true if the file has a valid YAML extension, otherwise it retruns false.
 */
function currentFileIsYaml(){
	let currentfilePath = "";
	let activeEditor = vscode.window.activeTextEditor;
	if (activeEditor) {
		currentfilePath = vscode.workspace.asRelativePath(
			activeEditor.document.uri
		);
	}
	return currentfilePath.endsWith(".yml") || 
		currentfilePath.endsWith(".yaml") ;
}

/**
 * This method is called when your extension is deactivated .
 */
function deactivate() {
	outputChannel.dispose();
	console.log('The Threagile extension is now not active!');
}

module.exports = {
	activate,
	deactivate
}
